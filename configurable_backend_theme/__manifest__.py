{
    'name': "Configurable Backend Theme",
    'summary': "Styles the backend_theme as defined in the config file",
    'version': "1.0",
    'author': "Solar21 AG",
    'website': "https://www.solar21.ch",
    'category': "",
    'depends': ['web_flectra'],
    'data': [],
    'installable': True,
    'auto_install': False,
    'license': 'LGPL-3',
    'application': False,
}
