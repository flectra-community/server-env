import logging

from flectra import api, models
from flectra.tools import config

_logger = logging.getLogger(__name__)


class ResCompany(models.Model):
    _inherit = "res.company"

    def _register_hook(self):
        super()._register_hook()

        backend_theme = self._get_backend_config()

        if not backend_theme:
            return

        # create a dictionary with all companies and styles
        styles_by_company = {}
        default_style = {}

        allowed_properties = {
            'theme_color_brand',
            'theme_background_color',
            'theme_font_name',
            'theme_sidebar_color',
            'google_font',
        }

        for key in backend_theme:
            style = key.split('-')[0]
            company_id_string = key.split('-')[-1]

            if company_id_string == 'default':
                if style in allowed_properties:
                    default_style[style] = backend_theme[key]

            else:
                company_id = int(company_id_string) if company_id_string.isdigit() else False
                if company_id and style in allowed_properties:
                    styles_by_company.setdefault(company_id, {})
                    styles_by_company[company_id][style] = backend_theme[key]

        # update the styles for each company
        # bundling by company is more efficient, as the call to write() will recreate the stylesheets
        env = api.Environment(self._cr, 1, {})
        all_companies = env['res.company'].search([])
        for company in all_companies:
            if company.id in styles_by_company:
                properties_to_overwrite = {}
                for key, val in styles_by_company[company.id].items():
                    if company[key] != val:
                        properties_to_overwrite[key] = val

                if properties_to_overwrite:
                    _logger.info("Updating styles for company %s with id %s", company.name, company.id)
                    company.write(properties_to_overwrite)

            elif default_style:
                properties_to_overwrite = {}
                for key, val in default_style.items():
                    if company[key] != val:
                        properties_to_overwrite[key] = val

                if properties_to_overwrite:
                    _logger.info("Updating styles for company %s with id %s", company.name, company.id)
                    company.write(properties_to_overwrite)

    def _get_backend_config(self, section='backend_theme'):
        # get the backend_theme configs from the config file
        backend_theme = config.misc.get(section)
        return backend_theme
