================================================================
Configurable Backend Theme
================================================================

.. |badge1| image:: https://img.shields.io/badge/licence-LGPL--3-blue.png
    :target: http://www.gnu.org/licenses/lgpl-3.0-standalone.html
    :alt: License: LGPL-3

|badge1|

Styles the backend_theme as defined in the config file. Can overwrite the following properties 'theme_color_brand', 'theme_background_color', 'theme_font_name', 'theme_sidebar_color', 'google_font'


**Table of contents**

.. contents::
   :local:

Installation
============

To install this module, you need to have the web_flectra module
installed and properly configured.

Configuration
=============
The id of the company, whose styles should be updated, has to be appended to the property name with a dash. E.g.: theme_color_brand-1.
Only the properties that are part of the config collection [backend_theme] are considered. The assets are only updated, if the config value is different from the current value.

To set theme parameters for all companies, use the suffix default. E.g.: theme_color_brand-default.
Example of config file ::

  [backend_theme]
  theme_color_brand-default = '#ff0000'

  theme_color_brand-1 = rgb(56, 212, 48)
  theme_background_color-1 = '#f2f7fb'

  theme_color_brand-2 = rgb(153, 43, 120)

Credits
=======

Change Logs
============

* [15-09-2023] ``fix`` fix that flectra doesn't crash if config isn't theme_color_brand

Authors
~~~~~~~

* Solar21 AG

Contributors
~~~~~~~~~~~~

* Daniel Rey <daniel.rey@solar21.ch>
* Renzo Meister <rm@jamotion.ch>

