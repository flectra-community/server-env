# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).
{
    "name": "Server Environment Data Encryption",
    "version": "2.0.1.0.2",
    "development_status": "Alpha",
    "category": "Tools",
    "website": "https://gitlab.com/flectra-community/server-env",
    "author": "Akretion, Odoo Community Association (OCA)",
    "license": "AGPL-3",
    "installable": True,
    "depends": ["server_environment", "data_encryption"],
}
