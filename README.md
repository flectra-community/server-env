# Flectra Community / server-env

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[server_environment_ir_config_parameter](server_environment_ir_config_parameter/) | 2.0.1.1.1|         Override System Parameters from server environment file
[server_environment_data_encryption](server_environment_data_encryption/) | 2.0.1.0.2| Server Environment Data Encryption
[auth_saml_environment](auth_saml_environment/) | 2.0.1.0.0| Allows system administrator to authenticate with any account
[mail_environment](mail_environment/) | 2.0.1.0.1| Configure mail servers with server_environment_files
[payment_environment](payment_environment/) | 2.0.1.0.2| Configure payment acquirers with server_environment
[server_environment](server_environment/) | 2.0.2.2.0| move some configurations out of the database
[mail_environment_office365](mail_environment_office365/) | 2.0.1.0.0|  Configure Office365 parameters with environment variables    via server_environment
[data_encryption](data_encryption/) | 2.0.1.0.0| Store accounts and credentials encrypted by environment
[server_environment_iap](server_environment_iap/) | 2.0.1.0.0| Configure IAP Account with server_environment_files
[server_environment_ir_config_parameter](server_environment_ir_config_parameter/) | 2.0.1.1.0| Override System Parameters from server environment file
[configurable_backend_theme](configurable_backend_theme/) | 2.0.0.0.0| Styles the backend_theme as defined in the config file


