========================================================
Backend theme configuration with server_environment
========================================================

This module allows to configure backend theme settings using server_environment files.

**Table of contents**

.. contents::
   :local:

Installation
============

To install this module, you need to have the server_environment module
installed and properly configured.

After installed, the environment configurable fields will be read from
the environment files.

Configuration
=============

With this module installed, the backend style params are configured in
the `server_environment_files` module (which is a module you should provide,
see the documentation of `server_environment` for more information).

In the configuration file of each environment, for each payment acquirer you
may use the section `[payment_acquirer.technical_name]` to configure the
acquirer values, where "technical_name" is the acquirer's `tech_name`.

Example of config file ::

  [backend_theme]
  theme_color_brand-default=#FF0000

Usage
=====

Once configured, Flectra will read the backend theme style settings from the
configuration file related to each environment defined in the main flectra file.

Credits
=======

Authors
~~~~~~~

* Jamotion GmbH

Contributors
~~~~~~~~~~~~

* `Jamotion GmbH <https://jamotion:ch>`_
