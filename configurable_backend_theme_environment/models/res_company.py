from flectra import fields, models
from flectra.addons.server_environment.server_env import serv_config


class ResCompany(models.Model):
    _inherit = "res.company"

    def _get_backend_config(self, section='backend_theme'):
        backend_theme = super(ResCompany, self)._get_backend_config(section)
        try:
            return dict(serv_config.items(section))
        except:
            pass

        return backend_theme
