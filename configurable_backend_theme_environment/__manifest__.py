{
    "name": "Configurable backend theme configuration with server_environment",
    "summary": "Configure backend theme with server_environment",
    "category": "Tools",
    "version": "2.0.1.0.2",
    "license": "GPL-3",
    "author": "Jamotion GmbH",
    "website": "https://jamotion.ch",
    "depends": ["configurable_backend_theme", "server_environment"],
    "data": [],
}
